<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Entity\Row;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/profile/dashboard", name="user")
     */
    public function index()
    {
        $id = $this->container->get('security.token_storage')->getToken()->getUser();

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id->getId());

        $invoice = $this->getDoctrine()
            ->getRepository(Invoice::class)
            ->findBy(['user' => $id->getId()]);

        return $this->render('user/index.html.twig', [
            'user' => $user,
            'invoice' => $invoice
        ]);
    }

    /**
     * @Route("/profile/dashboard/invoice/check/{id}/", name="user_see_invoice")
     */
    public function seeInvoiceAction($id){
        $invoice = $this->getDoctrine()
            ->getRepository(Invoice::class)
            ->findBy(['id' => $id]);

        $row = $this->getDoctrine()
            ->getRepository(Row::class)
            ->findBy(['invoice' => $id]);

        return $this->render('user/see_own_invoice.html.twig', [
            'invoice' => $invoice,
            'row' => $row
        ]);
    }
}
